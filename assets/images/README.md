TLS keys image from Benjamin Dowling, Marc Fischlin, Felix Günther, and Douglas
Stebila: A Cryptographic Analysis of the TLS 1.3 Handshake Protocol

<https://eprint.iacr.org/2020/1044.pdf>
