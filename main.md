---
title: Cryptography in your Average Web App
author: David Dervishi
date: December 10th, 2022
theme: moon
---

# Introduction

## I like

- cryptography
- information theory
- security proofs
- reductions
- protocols
- implementations

## But you don't {.nobullets}

. . .

- [$\checkmark$ design a web app]{.good}
- [✗ read the TLS 1.3 RFC]{.bad}

## Today {.nobullets}

- [✗ Hire someone else]{.bad}
- [✗ Use some magic framework]{.bad}
- [$\checkmark$ Conceptual understanding]{.good}
- [✗ Implement your own stuff]{.bad}

. . .

Take a look at the cryptographic concepts in a typical web app

# Connection

## Plain HTTP

![[Firefox is not happy]{.bad}](./assets/images/plain-http.png){ height=100% }

## TLS {.nobullets}

- [$\checkmark$ Secure connection]{.good}
- [✗ 160 pages of TLS 1.3 RFC, ~100 external references]{.bad}

## What do we need? {.boldtables}

| [Server authentication]{.bad} | [Certificates]{.good}     |
|-------------------------------|---------------------------|
| [Shared keys]{.bad}           | [DH]{.good}               |
| [Confidentiality]{.bad}       | [Symmetric cipher]{.good} |

## DH {.nobullets .iotables}

(EC)DH(E)

| In  | Group $\left<g\right>$<br/>Random $x, y$ |
|-----|------------------------------------------|
| Out | Uniform group element $g^{xy}$           |

. . .

- [$\checkmark$ Got a shared key]{.good}

## Symmetric cipher {.nobullets .iotables}

AES, ChaCha20

| In  | Uniform bitstring<br/>Message |
|-----|-------------------------------|
| Out | Uniform ciphertext            |

. . .

- [✗ Type check fails]{.bad}

. . .

- Uniform group element $\ne$ uniform bitstring

## Key derivation

Map our randomness to the required format

## KDF {.nobullets .iotables}

HKDF

| In  | Shared secret     |
|-----|-------------------|
| Out | Uniform bitstring |

. . .

- [$\checkmark$ Got a shared key]{.good}

## Never use a key for two purposes

![TLS keys](./assets/images/tls-keys.png){ height=100% }

## Communication {.nobullets}

| Alice              |                   | Bob                 |
|:------------------:|:-----------------:|:-------------------:|
|                    | *DH*              |                     |
|                    | *KDF*             |                     |
| Encrypt $m$ to $c$ | $\longrightarrow$ | Decrypt $c$ to $m'$ |

. . .

- [✗ And now $m'$ is garbage]{.bad}
- [✗ An on-path attacker flipped some bits]{.bad}

## Message authentication

Must authenticate messages after the key exchange

## MAC {.iotables}

HMAC, CBC-MAC, GMAC

| In  | Key<br/>Message |
|-----|-----------------|
| Out | Tag             |

. . .

TLS uses something else for messages

## AEAD {.nobullets .iotables}

AES-GCM, ChaCha20-Poly1305

| In  | Key<br/>Message<br/>Additional data |
|-----|-------------------------------------|
| Out | Ciphertext<br/>Tag                  |

. . .

- [$\checkmark$ Messages are authenticated]{.good}

## Got a secure channel, let's log in

# Sending passwords

## Plain over TLS

[$\checkmark$ Just send the thing over a secure channel]{.good}

## Passwordless authentication {.nobullets}

- [$\checkmark$ Hardware keys]{.good}: WebAuthn, FIDO2
- [$\checkmark$ PAKE]{.good}: SRP, OPAQUE

# Storing passwords

## Plaintext

. . .

[No.]{.bad}

## Hash the thing {.nobullets .iotables}

SHA-256, SHA3, BLAKE2

| In  | Password                              |
|-----|---------------------------------------|
| Out | Hard to reverse random-looking string |

. . .

- [✗ Dictionary attacks]{.bad}
- [✗ Rainbow tables]{.bad}

## Hash+Salt {.nobullets .iotables}

| In  | Password<br/>Unique per-password random string |
|-----|------------------------------------------------|
| Out | Hard to reverse random-looking string          |

. . .

- [$\checkmark$ Immune to offline precomputation]{.good}

## Data leak

. . .

[✗ Can still reverse salted hash for targeted users with 100s of GPUs]{.bad}

## Password hash function {.nobullets}

Argon2(i)(d), yescrypt

. . .

- Require a stupid amount of resources to compute a hash

. . .

- [$\checkmark$ Makes brute-forcing passwords stupidly inefficient]{.good}
- [✗ Potential DoS vector]{.bad}

# MFA

## Send a code over email {.nobullets}

- [Unencrypted]{.bad}

## Send a code over SMS {.nobullets}

- [Please stop]{.bad}

## HOTP {.nobullets .iotables}

SHA-1 by default

| In  | Shared secret key<br/>Shared counter          |
|-----|-----------------------------------------------|
| Out | Truncated hash output<br/>Incremented counter |

. . .

- [✗ Counter desynchronization]{.bad}

## TOTP {.nobullets .iotables}

SHA-1 by default

| In  | Shared secret key<br/>Current time |
|-----|------------------------------------|
| Out | Truncated hash output              |

. . .

- [$\checkmark$ Easy sync]{.good}

# Data at rest

## Plain {.nobullets}

- [✗ Data accessible if disk stolen]{.bad}

## Issues with disk encryption {.nobullets}

[Powerful adversary with full access to the disk]{.bad}

. . .

Individual data blocks:

- [$\checkmark$ Should be handled separately (performance)]{.good}
- [✗ Cannot be handled separately (security)]{.bad}

. . .

- [$\checkmark$ Should have an authentication tag (security)]{.good}
- [✗ Cannot have an authentication tag (storage)]{.bad}

## Ideas {.nobullets}

- [$\checkmark$ Encrypt sector-wise]{.good}

  [✗ How do we store IVs?]{.bad}

. . .

- [$\checkmark$ Authenticate sector-wise]{.good}

  [✗ Adversary could just replace the whole sector]{.bad}

## CBC-ESSIV {.iotables}

| In  | Key<br/>Sector number<br/>Sector |
|-----|----------------------------------|
| Out | Encrypted sector                 |

. . .

- IVs are encrypted sector numbers
- [Known plaintext attacks exist because of CBC]{.bad}

## XTS {.iotables}

| In  | Key<br/>Sector number<br/>Block number<br/>Block |
|-----|--------------------------------------------------|
| Out | Encrypted block                                  |

. . .

- IV from sector and block number
- Independent data blocks
- [Modification of a single block can go unnoticed]{.bad}

# Conclusion

## Cryptography is hard {.nobullets}

- [$\checkmark$ Primitives to solve (almost) any problem]{.good}
- [✗ Hard tradeoffs]{.bad}
- [✗ Threat models hard to establish]{.bad}

## The cryptography behind a good web app is stupidly hard

. . .

And we haven't even touched web security, or access control
